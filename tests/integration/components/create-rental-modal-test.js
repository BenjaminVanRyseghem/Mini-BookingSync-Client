import {moduleForComponent, test} from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import Ember from 'ember';

moduleForComponent('create-rental-modal', 'Integration | Component | create rental modal', {
  integration: true
});

test('it renders the button', function(assert) {
  this.render(hbs`{{create-rental-modal}}`);
  assert.equal(this.$().find('.new-rental-btn').text().trim(), 'Create a new rental');
});

test('it hides the modal by default', function(assert) {
  this.render(hbs`{{create-rental-modal}}`);
  assert.notOk(this.$().find('.modal').attr('class').trim().match('in'));
});

test('it shows the modal by clicking the button', function(assert) {
  let done = assert.async();
  this.set('rentalGenerator', () => {});
  this.render(hbs`{{create-rental-modal rentalGenerator=rentalGenerator}}`);

  Ember.run(() => document.querySelector('.new-rental-btn').click());

  setTimeout(() => {
    assert.ok(this.$().find('.modal').attr('class').trim().match('in'));
    done();
  }, 1000);// fade effect
});
