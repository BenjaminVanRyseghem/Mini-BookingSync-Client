import {moduleForComponent, test} from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import Ember from 'ember';

moduleForComponent('book-rental-modal', 'Integration | Component | book rental modal', {
  integration: true
});

test('it renders the button', function(assert) {
  this.render(hbs`{{book-rental-modal}}`);
  assert.equal(this.$().find('.book.btn').text().trim(), 'Book');
});

test('it hides the modal by default', function(assert) {
  this.render(hbs`{{book-rental-modal}}`);
  assert.notOk(this.$().find('.modal').attr('class').trim().match('in'));
});

test('it shows the modal by clicking the button', function(assert) {
  let done = assert.async();
  this.set('rental', {});
  this.set('bookingGenerator', () => {});
  this.render(hbs`{{book-rental-modal rental=rental bookingGenerator=bookingGenerator}}`);

  Ember.run(() => document.querySelector('.book').click());

  setTimeout(() => {
    assert.ok(this.$().find('.modal').attr('class').trim().match('in'));
    done();
  }, 1000);// fade effect
});
