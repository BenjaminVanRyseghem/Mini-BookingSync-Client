import Ember from 'ember';
import {moduleFor, test} from 'ember-qunit';

moduleFor('controller:rentals/show', 'Unit | Controller | rental | show', {});

test('it exists', function(assert) {
  let controller = this.subject();
  assert.ok(controller);
});

test('it has bookings', function(assert) {
  let controller = this.subject();
  Ember.run(function() {

    controller.set('model', Ember.Object.create({bookings: {content: [1, 2, 3]}}));
    assert.equal(controller.get('hasBookings'), true);
  });
});

test('it has no booking', function(assert) {
  let controller = this.subject();
  Ember.run(function() {

    controller.set('model', Ember.Object.create({bookings: {content: []}}));
    assert.equal(controller.get('hasBookings'), false);
  });
});

test('it has an imgId', function(assert) {
  let controller = this.subject();
  Ember.run(function() {
    let id = 8;

    controller.set('model', Ember.Object.create({id}));
    assert.equal(controller.get('imgId'), id);
  });
});
