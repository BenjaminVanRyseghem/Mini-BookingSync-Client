(function() {
  function vendorModule() {
    'use strict';

    return {
      'default': self['numbro'],
      __esModule: true,
    };
  }

  define('numbro', [], vendorModule);
})();
