# Mini-BookingSync-Client

[![pipeline status](https://gitlab.com/BenjaminVanRyseghem/Mini-BookingSync-Client/badges/master/pipeline.svg)](https://gitlab.com/BenjaminVanRyseghem/Mini-BookingSync-Client/commits/master)

Exercise made in the process of hiring. Assignment can be found [here](https://www.bookingsync.com/en/jobs/2017-04-03-senior-full-stack-rails-ember-js-developer).

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with NPM)
* [Ember CLI](https://ember-cli.com/)
* [PhantomJS](http://phantomjs.org/)

## Installation

* `git clone git@gitlab.com:BenjaminVanRyseghem/Mini-BookingSync-Client.git` this repository
* `cd Mini-Bookingsync-Client`
* `npm install`

## Running / Development

Make sur the [api](https://gitlab.com/BenjaminVanRyseghem/Mini-BookingSync-API) is up and running, on http://localhost:3000

* `npm start`
* Visit your app at [http://localhost:4200](http://localhost:4200).

### Running Tests

* `npm test`
