import Ember from 'ember';

export default Ember.Controller.extend({
  init: function() {
    this._super(...arguments);
    let home = this;
    this.set('bookingGenerator', function(data = {}) {
      return home.store.createRecord('booking', data);
    });
  },
  hasBookings: Ember.computed('model', function() {
    let bookings = this.get('model.bookings');
    return !!bookings.content.length;
  }),
  imgId: Ember.computed('model', function() {
    let id = this.get('model').id;
    return id % 10;
  })
});
