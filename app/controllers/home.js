import Ember from 'ember';

export default Ember.Controller.extend({
  init: function() {
    this._super(...arguments);
    let home = this;
    this.set('rentalGenerator', function(data = {}) {
      return home.store.createRecord('rental', data);
    });
  },
  hasRental: Ember.computed('model', function() {
    let rentals = this.get('model');
    return rentals.filter((rental) => {
      return !rental.pending;
    }).length;
  })
});
