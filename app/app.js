import Ember from 'ember';
import Resolver from './resolver';
import loadInitializers from 'ember-load-initializers';
import config from './config/environment';
import DS from 'ember-data';
import ActiveModelAdapter from 'active-model-adapter';

const App = Ember.Application.extend({
  modulePrefix: config.modulePrefix,
  podModulePrefix: config.podModulePrefix,
  Resolver
});

App.ApplicationAdapter = ActiveModelAdapter.extend({
  namespace: 'api/v1'
});

App.Store = DS.Store.extend({
  adapter: 'App.ApplicationAdapter'
});

loadInitializers(App, config.modulePrefix);

export default App;
