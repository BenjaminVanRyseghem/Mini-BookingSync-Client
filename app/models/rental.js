import DS from 'ember-data';
import {validator, buildValidations} from 'ember-cp-validations';

const Validations = buildValidations({
  dailyRate: [
    validator('presence', true),
    validator('number', {
      allowString: true
    })
  ],
  name: [
    validator('presence', true)
  ]
});

export default DS.Model.extend(Validations, {
  dailyRate: DS.attr('number'),
  name: DS.attr('string'),
  bookings: DS.hasMany('booking')
});
