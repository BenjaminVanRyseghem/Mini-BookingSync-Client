import DS from 'ember-data';

export default DS.Model.extend({
  startAt: DS.attr('isodate'),
  endAt: DS.attr('isodate'),
  price: DS.attr('number'),
  clientEmail: DS.attr('string'),
  rental: DS.belongsTo('rental')
});
