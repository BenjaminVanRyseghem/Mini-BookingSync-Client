import Ember from 'ember';
import numbro from 'numbro';

const defaultFormat = {
  spaceSeparated: false,
  optionalMantissa: true,
  totalLength: 0
};

export function numbroHelper([date, format = defaultFormat]/*, hash*/) {
  return numbro(date).formatCurrency(format);
}

export default Ember.Helper.helper(numbroHelper);
