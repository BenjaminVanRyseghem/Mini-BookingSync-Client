import Ember from 'ember';
import moment from 'moment';

export function momentHelper([date, format]/*, hash*/) {
  return moment(date).format(format);
}

export default Ember.Helper.helper(momentHelper);
