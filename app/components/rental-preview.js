import Ember from 'ember';

export default Ember.Component.extend({
  classNameBindings: ['isPending'],
  isPending: Ember.computed.alias('rental.pending'),
  modalOpened: false,
  numberOfBookings: Ember.computed('rental.bookings', function() {
    let bookings = this.get('rental.bookings');
    return bookings.content.length;
  }),
  imgId: Ember.computed('rental', function() {
    let id = this.get('rental').id;
    return id % 10;
  }),
  actions: {
    deleteRental: function(rental) {
      rental.destroyRecord();
    }
  }
});
