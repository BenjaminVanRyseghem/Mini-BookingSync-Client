import Ember from 'ember';
import numbro from 'numbro';

export default Ember.Component.extend({
  modalOpened: false,
  showPrice: Ember.computed('booking.startAt', 'booking.endAt', function() {
    let startAt = new Date(this.get('booking.startAt'));
    let endAt = new Date(this.get('booking.endAt'));
    return !isNaN(startAt.getDate()) && !isNaN(endAt.getDate());
  }),
  newPrice: Ember.computed('booking.startAt', 'booking.endAt', 'rental.dailyRate', function() {
    let startAt = new Date(this.get('booking.startAt'));
    let endAt = new Date(this.get('booking.endAt'));
    let dailyRate = this.get('rental.dailyRate');
    let newPrice = ((endAt - startAt) / (24 * 3600 * 1000)) * dailyRate;
    return numbro(newPrice).formatCurrency({
      spaceSeparated: false,
      optionalMantissa: true,
      totalLength: 0
    });
  }),
  actions: {
    showModal: function() {
      let rental = this.get('rental');
      let booking = this.get('bookingGenerator')({
        rental,
        pending: true
      });
      this.set('modalOpened', true);
      this.set('booking', booking);
    },
    cancelModal: function(booking) {
      this.set('modalOpened', false);
      booking.deleteRecord();
    },
    submit: function() {
      let booking = this.get('booking');
      let rental = this.get('rental');

      booking.set('pending', false);
      booking.save().then(() => {
        rental.hasMany('bookings').reload();
      });
      this.set('modalOpened', false);
    }
  }
});
