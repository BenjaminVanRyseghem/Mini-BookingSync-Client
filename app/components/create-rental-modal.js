import Ember from 'ember';

export default Ember.Component.extend({
  modalOpened: false,
  actions: {
    showModal: function() {
      let rental = this.get('rentalGenerator')({pending: true});
      this.set('modalOpened', true);
      this.set('rental', rental);
    },
    cancelModal: function(rental) {
      this.set('modalOpened', false);
      rental.deleteRecord();
    },
    submit: function() {
      let rental = this.get('rental');

      rental.set('pending', false);
      rental.save();
      this.set('modalOpened', false);
    }
  }
});
