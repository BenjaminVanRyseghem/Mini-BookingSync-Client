import Ember from 'ember';

export default Ember.Component.extend({
  editModalOpened: false,
  actions: {
    cancelEdition: function(rental) {
      rental.rollbackAttributes();
      this.set('editModalOpened', false);
    },
    submit: function() {
      let rental = this.get('model');
      rental.save().then(() => {
        rental.hasMany('bookings').reload();
      });

      this.set('editModalOpened', false);
    }
  }
});
