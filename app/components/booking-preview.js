import Ember from 'ember';
import numbro from 'numbro';

export default Ember.Component.extend({
  tagName: 'tr',
  classNameBindings: ['isPending'],
  isPending: Ember.computed.alias('booking.pending'),
  deleteModalOpened: false,
  editModalOpened: false,
  newPrice: Ember.computed('booking.startAt', 'booking.endAt', 'rental.dailyRate', function() {
    let startAt = new Date(this.get('booking.startAt'));
    let endAt = new Date(this.get('booking.endAt'));
    let dailyRate = this.get('rental.dailyRate');
    let newPrice = ((endAt - startAt) / (24 * 3600 * 1000)) * dailyRate;
    return numbro(newPrice).formatCurrency({
      spaceSeparated: false,
      optionalMantissa: true,
      totalLength: 0
    });
  }),
  actions: {
    deleteBooking: function(booking) {
      booking.destroyRecord();
    },
    cancelBookingEdition: function(booking, modal) {
      booking.rollbackAttributes();
      modal.close();
    },
    editBooking: function(booking) {
      booking.save();
      this.set('editModalOpened', false);
    }
  }
});
