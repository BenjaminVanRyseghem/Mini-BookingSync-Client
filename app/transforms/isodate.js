import DS from 'ember-data';
import moment from 'moment'

export default DS.Transform.extend({
  deserialize(serialized) {
    return moment(serialized).format('YYYY-MM-DD');
  },

  serialize(deserialized) {
    return JSON.stringify(new Date(deserialized));
  }
});
