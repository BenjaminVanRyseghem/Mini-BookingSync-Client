import DS from 'ember-data';

export default DS.JSONAPIAdapter.extend({
  namespace: 'api/v1',
  headers: {
    'Authorization': 'Token token="secret"'
  },
});
